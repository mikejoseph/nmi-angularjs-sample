﻿using System;
using System.Configuration;
using System.Web;
using System.Xml;
using Solupay.ThreeStep.Models;

namespace Solupay.ThreeStep
{
    public class SolupayXmlRequestBuilder
    {
        private string _apiKey;
        private XmlDocument _xmlRequest;

        public SolupayXmlRequestBuilder(string apiKey)
        {
            _apiKey = apiKey;
        }

        public XmlDocument GetCompleteTransactionRequest(string tokenId)
        {
            var xmlRequest = new XmlDocument();
            var xmlDecl = xmlRequest.CreateXmlDeclaration("1.0", "UTF-8", "yes");
            var root = xmlRequest.DocumentElement;
            xmlRequest.InsertBefore(xmlDecl, root);

            var xmlCompleteTransaction = xmlRequest.CreateElement("complete-action");
            var xmlApiKey = xmlRequest.CreateElement("api-key");
            xmlApiKey.InnerText = ConfigurationManager.AppSettings["SolupayApiKey"];
            xmlCompleteTransaction.AppendChild(xmlApiKey);

            var xmlTokenId = xmlRequest.CreateElement("token-id");
            xmlTokenId.InnerText = tokenId;
            xmlCompleteTransaction.AppendChild(xmlTokenId);
            xmlRequest.AppendChild(xmlCompleteTransaction);
            return xmlRequest;
        }

        public XmlDocument GetTransactionStep1Request(Step1Model model)
        {
            var xmlTransaction = InitializeRequest(model.TransactionType, model);
            if (model.TransactionType != "vault")
            {
                AddOrderInformation(model, xmlTransaction);
            }

            ProcessCustomerVaultInfo(model, xmlTransaction);
            AppendBillingInfo(model, xmlTransaction);
            AppendShippingInfo(model, xmlTransaction);
            AppendProductInformation(xmlTransaction);
            _xmlRequest.AppendChild(xmlTransaction);
            return _xmlRequest;
        }
        
        private XmlElement InitializeRequest(string transactionType, Step1Model model)
        {
            _xmlRequest = new XmlDocument();
            var xmlDeclaration = _xmlRequest.CreateXmlDeclaration("1.0", "UTF-8", "yes");
            var root = _xmlRequest.DocumentElement;
            _xmlRequest.InsertBefore(xmlDeclaration, root);

            var rootElementName = transactionType;
            if (transactionType == "vault")
            {
                if (model.CustomerVaultMode == "create")
                {
                    rootElementName = "add-customer";
                }
                else if (model.CustomerVaultMode == "update")
                {
                    rootElementName = "update-customer";
                }
                else
                {
                    throw new Exception($"Unsupported Mode: {model.CustomerVaultMode}");
                }
            }

            var xmlTransaction = _xmlRequest.CreateElement(rootElementName);
            var xmlApiKey = _xmlRequest.CreateElement("api-key");
            xmlApiKey.InnerText = _apiKey;
            xmlTransaction.AppendChild(xmlApiKey);

            var xmlRedirectUrl = _xmlRequest.CreateElement("redirect-url");
            xmlRedirectUrl.InnerText = model.Step2RedirectUrl;
            xmlTransaction.AppendChild(xmlRedirectUrl);

            var xmlMdf1 = _xmlRequest.CreateElement("merchant-defined-field-1");
            xmlMdf1.InnerText = model.MerchantDefinedField1;
            xmlTransaction.AppendChild(xmlMdf1);

            return xmlTransaction;
        }

        private void AddOrderInformation(Step1Model model, XmlElement xmlTransaction)
        {
            var xmlAmount = _xmlRequest.CreateElement("amount");
            xmlAmount.InnerText = model.OrderInfo.TotalAmount.ToString("N").Replace(",", "");
            xmlTransaction.AppendChild(xmlAmount);

            var xmlRemoteAddr = _xmlRequest.CreateElement("ip-address");
            xmlRemoteAddr.InnerText = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            xmlTransaction.AppendChild(xmlRemoteAddr);

            var xmlCurrency = _xmlRequest.CreateElement("currency");
            xmlCurrency.InnerText = "USD";
            xmlTransaction.AppendChild(xmlCurrency);

            var xmlOrderId = _xmlRequest.CreateElement("order-id");
            xmlOrderId.InnerText = model.OrderInfo.OrderId;
            xmlTransaction.AppendChild(xmlOrderId);

            var xmlOrderDescription = _xmlRequest.CreateElement("order-description");
            xmlOrderDescription.InnerText = model.OrderInfo.OrderDescription;
            xmlTransaction.AppendChild(xmlOrderDescription);

            var xmlMdf2 = _xmlRequest.CreateElement("merchant-defined-field-2");
            xmlMdf2.InnerText = model.MerchantDefinedField2;
            xmlTransaction.AppendChild(xmlMdf2);

            var xmlTax = _xmlRequest.CreateElement("tax-amount");
            xmlTax.InnerText = model.OrderInfo.TaxAmount.ToString("N").Replace(",", "");
            xmlTransaction.AppendChild(xmlTax);

            var xmlShipping = _xmlRequest.CreateElement("shipping-amount");
            xmlShipping.InnerText = model.OrderInfo.ShippingAmount.ToString("N").Replace(",", "");
            xmlTransaction.AppendChild(xmlShipping);
        }

        private void ProcessCustomerVaultInfo(Step1Model model, XmlElement xmlTransaction)
        {
            if (model.TransactionType == "vault")
            {
                switch (model.CustomerVaultMode)
                {
                    case "create":
                    case "update":
                        var xmlCustomerVaultId = _xmlRequest.CreateElement("customer-vault-id");
                        xmlCustomerVaultId.InnerText = model.CustomerVaultId;
                        xmlTransaction.AppendChild(xmlCustomerVaultId);
                        break;
                    default:
                        throw new Exception($"Unexpected Mode: {model.CustomerVaultMode}");
                }
            }
            else
            {
                switch (model.CustomerVaultMode)
                {
                    case "create":
                        var xmlAddCustomer = _xmlRequest.CreateElement("add-customer");
                        var xmlAddCustomerVaultId = _xmlRequest.CreateElement("customer-vault-id");
                        xmlAddCustomerVaultId.InnerText = model.CustomerVaultId;
                        xmlAddCustomer.AppendChild(xmlAddCustomerVaultId);
                        xmlTransaction.AppendChild(xmlAddCustomer);
                        break;
                    case "update":
                        var xmlUpdateCustomer = _xmlRequest.CreateElement("update-customer");
                        var xmlUpdateCustomerVaultId = _xmlRequest.CreateElement("customer-vault-id");
                        xmlUpdateCustomerVaultId.InnerText = model.CustomerVaultId;
                        xmlUpdateCustomer.AppendChild(xmlUpdateCustomerVaultId);
                        xmlTransaction.AppendChild(xmlUpdateCustomer);
                        break;
                    case "existing":
                        var xmlCustomerVaultId = _xmlRequest.CreateElement("customer-vault-id");
                        xmlCustomerVaultId.InnerText = model.CustomerVaultId;
                        xmlTransaction.AppendChild(xmlCustomerVaultId);
                        break;
                    default:
                        var xmlNoCustomerVaultId = _xmlRequest.CreateElement("customer-vault-id");
                        xmlNoCustomerVaultId.InnerText = "";
                        xmlTransaction.AppendChild(xmlNoCustomerVaultId);
                        break;
                }
            }
        }

        private void AppendBillingInfo(Step1Model model, XmlElement xmlTransaction)
        {
            var xmlBillingAddress = _xmlRequest.CreateElement("billing");
            var xmlFirstName = _xmlRequest.CreateElement("first-name");
            xmlFirstName.InnerText = model.BillingInfo.FirstName;
            xmlBillingAddress.AppendChild(xmlFirstName);

            var xmlLastName = _xmlRequest.CreateElement("last-name");
            xmlLastName.InnerText = model.BillingInfo.LastName;
            xmlBillingAddress.AppendChild(xmlLastName);

            var xmlAddress1 = _xmlRequest.CreateElement("address1");
            xmlAddress1.InnerText = model.BillingInfo.Address;
            xmlBillingAddress.AppendChild(xmlAddress1);

            var xmlCity = _xmlRequest.CreateElement("city");
            xmlCity.InnerText = model.BillingInfo.City;
            xmlBillingAddress.AppendChild(xmlCity);

            var xmlState = _xmlRequest.CreateElement("state");
            xmlState.InnerText = model.BillingInfo.State;
            xmlBillingAddress.AppendChild(xmlState);

            var xmlZip = _xmlRequest.CreateElement("postal");
            xmlZip.InnerText = model.BillingInfo.Postal;
            xmlBillingAddress.AppendChild(xmlZip);

            var xmlCountry = _xmlRequest.CreateElement("country");
            xmlCountry.InnerText = model.BillingInfo.Country;
            xmlBillingAddress.AppendChild(xmlCountry);

            var xmlPhone = _xmlRequest.CreateElement("phone");
            xmlPhone.InnerText = model.BillingInfo.Phone;
            xmlBillingAddress.AppendChild(xmlPhone);

            var xmlEmail = _xmlRequest.CreateElement("email");
            xmlEmail.InnerText = model.BillingInfo.Email;
            xmlBillingAddress.AppendChild(xmlEmail);

            var xmlCompany = _xmlRequest.CreateElement("company");
            xmlCompany.InnerText = model.Company;
            xmlBillingAddress.AppendChild(xmlCompany);

            var xmlAddress2 = _xmlRequest.CreateElement("address2");
            xmlAddress2.InnerText = model.BillingInfo.Address2;
            xmlBillingAddress.AppendChild(xmlAddress2);

            var xmlFax = _xmlRequest.CreateElement("fax");
            xmlFax.InnerText = "";
            xmlBillingAddress.AppendChild(xmlFax);

            xmlTransaction.AppendChild(xmlBillingAddress);
        }

        private void AppendShippingInfo(Step1Model model, XmlElement xmlTransaction)
        {
            if (model.ShippingInfo == null) return;
            var xmlShippingAddress = _xmlRequest.CreateElement("shipping");

            var xmlSFirstName = _xmlRequest.CreateElement("first-name");
            xmlSFirstName.InnerText = model.ShippingInfo.FirstName;
            xmlShippingAddress.AppendChild(xmlSFirstName);

            var xmlSLastName = _xmlRequest.CreateElement("last-name");
            xmlSLastName.InnerText = model.ShippingInfo.LastName;
            xmlShippingAddress.AppendChild(xmlSLastName);

            var xmlSAddress1 = _xmlRequest.CreateElement("address1");
            xmlSAddress1.InnerText = model.ShippingInfo.Address;
            xmlShippingAddress.AppendChild(xmlSAddress1);

            var xmlSAddress2 = _xmlRequest.CreateElement("address2");
            xmlSAddress2.InnerText = model.ShippingInfo.Address2;
            xmlShippingAddress.AppendChild(xmlSAddress2);

            var xmlSCity = _xmlRequest.CreateElement("city");
            xmlSCity.InnerText = model.ShippingInfo.City;
            xmlShippingAddress.AppendChild(xmlSCity);

            var xmlSState = _xmlRequest.CreateElement("state");
            xmlSState.InnerText = model.ShippingInfo.State;
            xmlShippingAddress.AppendChild(xmlSState);

            var xmlSZip = _xmlRequest.CreateElement("postal");
            xmlSZip.InnerText = model.ShippingInfo.Postal;
            xmlShippingAddress.AppendChild(xmlSZip);

            var xmlSCountry = _xmlRequest.CreateElement("country");
            xmlSCountry.InnerText = model.ShippingInfo.Country;
            xmlShippingAddress.AppendChild(xmlSCountry);

            var xmlSPhone = _xmlRequest.CreateElement("phone");
            xmlSPhone.InnerText = model.ShippingInfo.Phone;
            xmlShippingAddress.AppendChild(xmlSPhone);

            var xmlEmail = _xmlRequest.CreateElement("email");
            xmlEmail.InnerText = model.BillingInfo.Email;
            xmlShippingAddress.AppendChild(xmlEmail);

            var xmlSFax = _xmlRequest.CreateElement("fax");
            xmlSFax.InnerText = "";
            xmlShippingAddress.AppendChild(xmlSFax);

            xmlTransaction.AppendChild(xmlShippingAddress);
        }

        private void AppendProductInformation(XmlElement xmlTransaction)
        {
            #region If you want product information to be included, you can use this region of code.

            //////////////////

            //var xmlProduct = _xmlRequest.CreateElement("product");

            //var xmlSku = _xmlRequest.CreateElement("product-code");
            //xmlSku.InnerText = "SKU-123456";
            //xmlProduct.AppendChild(xmlSku);

            //var xmlDescription = _xmlRequest.CreateElement("description");
            //xmlDescription.InnerText = "Books";
            //xmlProduct.AppendChild(xmlDescription);

            //var xmlQuantity = _xmlRequest.CreateElement("quantity");
            //xmlQuantity.InnerText = "1";
            //xmlProduct.AppendChild(xmlQuantity);

            //var xmlUnit = _xmlRequest.CreateElement("unit-of-measure");
            //xmlUnit.InnerText = "1";
            //xmlProduct.AppendChild(xmlUnit);

            //var xmlUnitAmount = _xmlRequest.CreateElement("total-amount");
            //xmlUnitAmount.InnerText = "1";
            //xmlProduct.AppendChild(xmlUnitAmount);

            //var xmlUnitDiscount = _xmlRequest.CreateElement("discount-amount");
            //xmlUnitDiscount.InnerText = "0.00";
            //xmlProduct.AppendChild(xmlUnitDiscount);


            //var xmlUnitTax = _xmlRequest.CreateElement("tax-amount");
            //xmlUnitTax.InnerText = "0.00";
            //xmlProduct.AppendChild(xmlUnitTax);

            //var xmlTaxRate = _xmlRequest.CreateElement("tax-rate");
            //xmlTaxRate.InnerText = "0.01";
            //xmlProduct.AppendChild(xmlTaxRate);

            //xmlTransaction.AppendChild(xmlProduct);

            /////////////////

            //var xmlProduct2 = _xmlRequest.CreateElement("product");

            //var xmlSku2 = _xmlRequest.CreateElement("product-code");
            //xmlSku2.InnerText = "SKU-654321";
            //xmlProduct2.AppendChild(xmlSku2);

            //var xmlDescription2 = _xmlRequest.CreateElement("description");
            //xmlDescription2.InnerText = "Videos";
            //xmlProduct2.AppendChild(xmlDescription2);

            //var xmlQuantity2 = _xmlRequest.CreateElement("quantity");
            //xmlQuantity2.InnerText = "1";
            //xmlProduct2.AppendChild(xmlQuantity2);

            //var xmlUnit2 = _xmlRequest.CreateElement("unit-of-measure");
            //xmlUnit2.InnerText = "";
            //xmlProduct2.AppendChild(xmlUnit2);

            //var xmlUnitAmount2 = _xmlRequest.CreateElement("total-amount");
            //xmlUnitAmount2.InnerText = "2";
            //xmlProduct2.AppendChild(xmlUnitAmount2);

            //var xmlUnitDiscount2 = _xmlRequest.CreateElement("discount-amount");
            //xmlUnitDiscount2.InnerText = "0.00";
            //xmlProduct2.AppendChild(xmlUnitDiscount2);


            //var xmlUnitTax2 = _xmlRequest.CreateElement("tax-amount");
            //xmlUnitTax2.InnerText = "0.00";
            //xmlProduct2.AppendChild(xmlUnitTax2);


            //var xmlTaxRate2 = _xmlRequest.CreateElement("tax-rate");
            //xmlTaxRate2.InnerText = "0.01";
            //xmlProduct2.AppendChild(xmlTaxRate2);

            //xmlTransaction.AppendChild(xmlProduct2);

            #endregion
        }
    }
}