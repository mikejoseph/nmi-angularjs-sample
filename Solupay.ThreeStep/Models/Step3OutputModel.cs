﻿using System.Xml.Serialization;

namespace Solupay.ThreeStep.Models
{
    /// <summary>
    /// This class will capture the Transaction XML response in Step 3 of the documentation:
    /// https://secure.networkmerchants.com/gw/merchants/resources/integration/integration_portal.php#transactions_step_3
    /// 
    /// I am only serializing the properties I need. You can expand upon this to read other elements 
    /// from the response if you wish.
    /// </summary>
    [XmlRoot("response")]
    public class Step3OutputModel
    {
        /// <summary>
        /// 1=Approved
        /// 2=Declined
        /// 3=Error in transaction data or system error
        /// </summary>
        [XmlElement("result")]
        public int Result { get; set; }

        [XmlElement("result-text")]
        public string ResultText { get; set; }

        [XmlElement("transaction-id")]
        public string TransactionId { get; set; }

        /// <summary>
        /// The result code:
        /// https://secure.networkmerchants.com/gw/merchants/resources/integration/integration_portal.php#3step_appendix_3
        /// </summary>
        [XmlElement("result-code")]
        public string ResultCode { get; set; }
        
        /// <summary>
        /// Transaction auth code.
        /// </summary>
        [XmlElement("authorization-code")]
        public string AuthorizationCode { get; set; }

        /// <summary>
        /// AVS response code. (See Appendix 1)
        /// https://secure.networkmerchants.com/gw/merchants/resources/integration/integration_portal.php#3step_appendix_1
        /// </summary>
        [XmlElement("avs-result")]
        public string AvsResult { get; set; }

        /// <summary>
        /// CVV response code. (See Appendix 2)
        /// https://secure.networkmerchants.com/gw/merchants/resources/integration/integration_portal.php#3step_appendix_2
        /// </summary>
        [XmlElement("cvv-result")]
        public string CvvResult { get; set; }

        /// <summary>
        /// Action type that was initially specified.
        /// Values: 'sale', 'auth', 'credit', 'validate', or 'offline'
        /// </summary>
        [XmlElement("action-type")]
        public string ActionType { get; set; }

        /// <summary>
        /// Total amount charged.
        /// </summary>
        [XmlElement("amount")]
        public decimal Amount { get; set; }

        /// <summary>
        /// Total amount authorized.
        /// </summary>
        [XmlElement("amount-authorized")]
        public decimal AmountAuthorized { get; set; }

        /// <summary>
        /// Solupay Internal Account ID
        /// </summary>
        [XmlElement("customer-vault-id")]
        public string CustomerVaultId { get; set; }

        /// <summary>
        /// Solupay Internal Account ID
        /// </summary>
        [XmlElement("customer-id")]
        public string CustomerId { get; set; }

        /// <summary>
        /// Order ID from our end
        /// </summary>
        [XmlElement("order-id")]
        public string OrderId { get; set; }

        /// <summary>
        /// Processor transaction was made through.
        /// </summary>
        [XmlElement("processor-id")]
        public string ProcessorId { get; set; }

        /// <summary>
        /// Payment descriptor
        /// </summary>
        [XmlElement("descriptor")]
        public string Descriptor { get; set; }

        /// <summary>
        /// External Account ID
        /// </summary>
        [XmlElement("merchant-defined-field-1")]
        public string MerchantDefinedField1 { get; set; }
        
        [XmlElement("merchant-defined-field-2")]
        public string MerchantDefinedField2 { get; set; }

        [XmlElement("billing")]
        public BillingInfo BillingInfo { get; set; }
    }
}
