﻿namespace Solupay.ThreeStep.Models
{
    public class OrderInfo
    {
        public decimal TotalAmount { get; set; }

        public string Currency { get; set; }

        public string OrderId { get; set; }

        public string OrderDescription { get; set; }
        
        public decimal TaxAmount { get; set; }

        public decimal ShippingAmount { get; set; }
    }
}
