﻿using System.Xml.Serialization;

namespace Solupay.ThreeStep.Models
{
    [XmlRoot("billing")]
    public class BillingInfo
    {
        [XmlElement("billing-id")]
        public string BillingId { get; set; }

        [XmlElement("first-name")]
        public string FirstName { get; set; }
        
        [XmlElement("last-name")]
        public string LastName { get; set; }

        [XmlElement("address1")]
        public string Address { get; set; }

        [XmlElement("city")]
        public string City { get; set; }

        [XmlElement("state")]
        public string State { get; set; }

        [XmlElement("postal")]
        public string Postal { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }

        [XmlElement("phone")]
        public string Phone { get; set; }

        [XmlElement("email")]
        public string Email { get; set; }

        [XmlElement("company")]
        public string Company { get; set; }

        [XmlElement("address2")]
        public string Address2 { get; set; }

        [XmlElement("fax")]
        public string Fax { get; set; }

        [XmlElement("account-type")]
        public string AccountType { get; set; }

        [XmlElement("entity-type")]
        public string EntityType { get; set; }

        [XmlElement("priority")]
        public int Priority { get; set; }

        [XmlElement("cc-number")]
        public string CardNumberOutput { get; set; }

        [XmlElement("cc-exp")]
        public string CardExpirationOutput { get; set; }

        [XmlElement("account-name")]
        public string AccountNameOutput { get; set; }

        [XmlElement("routing-number")]
        public string AccountRoutingNumberOutput { get; set; }

        [XmlElement("account-number")]
        public string AccountAccountNumberOutput { get; set; }
    }
}