﻿
using System.Xml.Serialization;

namespace Solupay.ThreeStep.Models
{
    [XmlRoot("response")]
    public class Step1OutputModel
    {
        /// <summary>
        /// 1=Approved
        /// 2=Declined
        /// 3=Error in transaction data or system error
        /// </summary>
        [XmlElement("result")]
        public int Result { get; set; }

        [XmlElement("result-text")]
        public string ResultText { get; set; }

        [XmlElement("transaction-id")]
        public string TransactionId { get; set; }

        /// <summary>
        /// The result code:
        /// https://secure.networkmerchants.com/gw/merchants/resources/integration/integration_portal.php#3step_appendix_3
        /// </summary>
        [XmlElement("result-code")]
        public string ResultCode { get; set; }

        [XmlElement("form-url")]
        public string FormUrl { get; set; }
    }
}