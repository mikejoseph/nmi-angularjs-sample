﻿namespace Solupay.ThreeStep.Models
{
    public class Step1Model
    {
        /// <summary>
        /// Valid Values: sale|auth|credit|validate|offline - Added 'vault' transaction type for customer vault operations only
        /// </summary>
        public string TransactionType { get; set; }

        public string Step2RedirectUrl { get; set; }

        /// <summary>
        /// Valid Values: none|create|update|existing
        /// Default: none
        /// </summary>
        public string CustomerVaultMode { get; set; }

        public string CustomerVaultId { get; set; }

        public string MerchantDefinedField1 { get; set; }

        public string MerchantDefinedField2 { get; set; }

        /// <summary>
        /// Only use this with a transaction.
        /// </summary>
        public OrderInfo OrderInfo { get; set; }

        public BillingInfo BillingInfo { get; set; }

        public ShippingInfo ShippingInfo { get; set; }

        public string Company { get; set; }
    }
}