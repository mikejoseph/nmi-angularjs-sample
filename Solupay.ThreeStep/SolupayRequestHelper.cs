﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Xml;

namespace Solupay.ThreeStep
{
    public class SolupayRequestHelper
    {
        public static string SendXmlRequest(XmlDocument xmlRequest)
        {
            if (HttpContext.Current.Request.ServerVariables["HTTPS"] == "ON") SslValidator.OverrideValidation();
            var uri = ConfigurationManager.AppSettings["SolupayGatewayPostUrl"];

            var req = WebRequest.Create(uri);
            req.Method = "POST";
            req.ContentType = "text/xml";

            // Wrap the request stream with a text-based writer
            var writer = new StreamWriter(req.GetRequestStream());

            // Write the XML text into the stream
            xmlRequest.Save(writer);
            writer.Close();

            // Send the data to the webserver
            var rsp = req.GetResponse();

            var dataStream = rsp.GetResponseStream();
            if (dataStream == null) throw new Exception("No response was received.");

            // Open the stream using a StreamReader 
            var reader = new StreamReader(dataStream);

            // Read the content.
            var responseFromServer = reader.ReadToEnd();
            
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            rsp.Close();

            return responseFromServer;
        }

        public static class SslValidator
        {
            private static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain,
                SslPolicyErrors sslPolicyErrors)
            {
                return true;
            }
            public static void OverrideValidation()
            {
                ServicePointManager.ServerCertificateValidationCallback =
                    OnValidateCertificate;
                ServicePointManager.Expect100Continue = true;
            }
        }
    }
}