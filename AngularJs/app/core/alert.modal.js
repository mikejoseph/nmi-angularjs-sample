﻿(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('alertModal', alertModal);

    alertModal.$inject = ['$uibModal'];

    function alertModal($uibModal) {
        return function (message, title, overrideOptions) {
            var options = {
                template: `
                    <div>
                        <div class="modal-header">
                            <h3 class="modal-title">{{vm.showTitle()}}</h3>
                        </div>
                        <div class="modal-body">
                            <div ng-bind-html="vm.message"></div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" ng-click="$close()">OK</button>
                        </div>
                    </div>`,
                controller: ['$scope', '$uibModalInstance',
                    function ($scope, $uibModalInstance) {
                        var vm = this;
                        vm.message = message;

                        vm.showTitle = function () {
                            if (title) return title;
                            return 'Alert message';
                        };
                    }],
                controllerAs: 'vm'
            };

            angular.extend(options, overrideOptions);
            return $uibModal.open(options).result;
        };
    }
})();
