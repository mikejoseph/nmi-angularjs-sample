(function () {
    'use strict';

    angular.module('app').factory('datacontext', ['config', '$http', datacontext]);

    function datacontext(config, $http) {
        var apiUrl = config.apiUrl;

        var service = {
            getResource: getResource,
            saveResource: saveResource,
            deleteResource: deleteResource,
            postResource: postResource,
            putResource: putResource
        };

        return service;

        function getResource(controllerName, objectId, additionalQueryStringParameters) {
            var url = apiUrl + controllerName;
            if (objectId) url += '/' + encodeURIComponent(objectId);

            if (additionalQueryStringParameters) {
                if (additionalQueryStringParameters.indexOf('?') === -1) url += '?';
                url += additionalQueryStringParameters;
            }

            return $http.get(url).then(function (response) { return response.data; });
        }

        function saveResource(controllerName, object) {
            var url = apiUrl + controllerName;

            if (object.id) {
                url += '/' + object.id;
                return $http.put(url, object).then(function (response) { return response.data; });
            } else {
                return $http.post(url, object).then(function (response) { return response.data; });
            }
        }

        function deleteResource(controllerName, objectId) {
            var url = apiUrl + controllerName;
            if (objectId) url += '/' + objectId;
            return $http.delete(url).then(function () { return true; });
        }

        function postResource(controllerName, postData) {
            var url = apiUrl + controllerName;
            return $http.post(url, postData).then(function (response) { return response.data; });
        }

        function putResource(controllerName, putData) {
            var url = apiUrl + controllerName;
            return $http.put(url, putData).then(function (response) { return response.data; });
        }
    }
})();
