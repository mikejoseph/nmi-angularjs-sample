﻿(function () {
    'use strict';

    var serviceId = 'localStorage';
    angular.module('app.core').factory(serviceId, ['$window', localStorage]);

    function localStorage($window) {
        var store = $window.localStorage;

        var service = {
            add: add,
            remove: remove,
            get: get
        };

        return service;

        function add(key, value) {
            value = angular.toJson(value);
            store.setItem(key, value);
        }

        function remove(key) {
            store.removeItem(key);
        }

        function get(key) {
            var value = store.getItem(key);
            if (value) value = angular.fromJson(value);
            return value;
        }
    }
})();
