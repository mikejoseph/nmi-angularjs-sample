﻿(function () {
    'use strict';

    angular
        .module('app.solupay')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'transaction',
                config: {
                    url: '/solupay/transaction',
                    template: '<solupay-transaction></solupay-transaction>',
                    title: 'Transaction Example',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-money"></i> Transaction Example'
                    }
                }
            }, {
                state: 'threeStepResult',
                config: {
                    url: '/solupay/threeStepResult/:tokenId',
                    template: '<three-step-result></three-step-result>',
                    title: 'Step Three',
                    settings: {
                        nav: 0,
                        content: '<i class="fa fa-dashboard"></i> Step Three'
                    }
                }
            }, {
                state: 'customerVault',
                config: {
                    url: '/solupay/customerVault',
                    template: '<customer-vault></customer-vault>',
                    title: 'Customer Example',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-lock"></i> Customer Vault Example'
                    }
                }
            }, {
                state: 'query',
                config: {
                    url: '/solupay/query',
                    template: '<query></query>',
                    title: 'Query Example',
                    settings: {
                        nav: 3,
                        content: '<i class="fa fa-search"></i> Query Example'
                    }
                }
            }, {
                state: 'viewLogs',
                config: {
                    url: '/solupay/viewLogs',
                    template: '<view-logs></view-logs>',
                    title: 'View Logs',
                    settings: {
                        nav: 4,
                        content: '<i class="fa fa-book"></i> View Logs'
                    }
                }
            }, {
                state: 'direct',
                config: {
                    url: '/solupay/direct',
                    template: '<direct></direct>',
                    title: 'Direct API Example',
                    settings: {
                        nav: 5,
                        content: '<i class="fa fa-money"></i> Direct API Example'
                    }
                }
            }
        ];
    }
})();
