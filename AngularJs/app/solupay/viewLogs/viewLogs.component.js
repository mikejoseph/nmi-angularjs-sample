﻿(function () {
    'use strict';

    angular
        .module('app.solupay')
        .component('viewLogs', {
            templateUrl: 'app/solupay/viewLogs/viewLogs.component.html',
            bindings: {},
            controllerAs: 'vm',
            controller: ['alertModal', 'datacontext', 'logger', controller]
        });

    function controller(alertModal, datacontext, logger) {
        var vm = this;
        vm.viewLog = viewLog;
        vm.clearLogs = clearLogs;
        vm.logs = [];
        
        vm.$onInit = function () {
            refresh();
        };

        function refresh() {
            datacontext.getResource('SolupayTransaction/TransactionLog').then(function(data) {
                vm.logs = data;
                logger.success('Payment logs were successfully retrieved.');
            });
        }

        function viewLog(log) {
            datacontext.getResource('SolupayTransaction/TransactionLog?tokenId=' + log.tokenId).then(function(data) {
                var html = '<pre>' + JSON.stringify(data, null, 4) + '</pre>';
                alertModal(html, 'Viewing Payment Transaction Log');
            });
        }

        function clearLogs() {
            if (window.confirm('Are you sure you want to delete the payment logs?')) {
                datacontext.deleteResource('Transaction/TransactionLog').then(function () {
                    logger.info('The transaction logs were cleared.');
                    refresh();
                });
            }
        }
    }
}());
