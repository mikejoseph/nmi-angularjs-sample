﻿(function () {
    'use strict';

    angular
        .module('app.solupay')
        .component('customerVaultControl', {
            template: `
                <div class="form-group">
                    <label class="col-sm-3">Customer Vault Mode</label>
                    <select ng-model="vm.customerVaultMode" ng-change="vm.onVaultModeChanged()">
                        <option value="none">Don't use a customer vault</option>
                        <option value="create">Create new vault</option>
                        <option value="update">Update existing vault</option>
                        <option value="existing">Use existing value</option>
                    </select>
                </div>

                <div class="form-group" ng-if="vm.customerVaultMode !== 'none'">
                    <label class="col-sm-3">Customer Vault ID</label>
                    <input type="text" class="form-control" ng-model="vm.customerVaultId">
                    <button class="btn btn-link btn-xs"
                            ng-click="vm.generateNewVaultId()"
                            ng-if="vm.customerVaultMode == 'create'">
                        New Vault ID
                    </button>
                </div>`,
            bindings: {
                customerVaultId: '=',
                customerVaultMode: '='
            },
            controllerAs: 'vm',
            controller: ['logger', 'threeStepService', controller]
        });

    function controller(logger, threeStepService) {
        var vm = this;
        var existingVaultId = '9f532462-1dfb-4d41-9862-c5e0da1614d5';

        vm.onVaultModeChanged = function () {
            switch (vm.customerVaultMode) {
            case 'create':
                    vm.customerVaultId = threeStepService.getNewGuid();
                logger.info('A new customer vault ID number was generated.');
                break;
            case 'update':
                vm.customerVaultId = existingVaultId;
                logger.info('An existing customer vault ID number was prefilled.');
                break;
            case 'existing':
                vm.customerVaultId = existingVaultId;
                logger.info('An existing customer vault ID number was prefilled.');
                break;
            default:
                vm.customerVaultId = null;
                logger.info('The customer vault ID number was cleared.');
                break;
            }
        };

        vm.generateNewVaultId = function () {
            vm.customerVaultId = threeStepService.getNewGuid();
            logger.info('A new customer vault ID number was generated.');
        };
    }
}());
