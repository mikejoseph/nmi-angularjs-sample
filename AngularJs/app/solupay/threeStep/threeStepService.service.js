﻿(function () {
    'use strict';

    var serviceId = 'threeStepService';
    angular.module('app.solupay').factory(serviceId, ['datacontext', threeStepService]);

    function threeStepService(datacontext) {
        var service = {
            getMockTransactionData: getMockTransactionData,
            getNewGuid: uuidv4
        };

        return service;

        function getMockTransactionData() {
            var retVal = {
                transactionType: 'sale',
                customerVaultMode: 'none',
                company: 'Test Company',
                billingInfo: {
                    firstName: 'John',
                    lastName: 'Smith',
                    address: '1234 Main St.',
                    city: 'Beverly Hills',
                    state: 'CA',
                    postal: '90210',
                    country: 'US',
                    phone: '555-555-5555',
                    email: 'test@example.com'
                },
                shippingInfo: {
                    firstName: 'Jane',
                    lastName: 'Smith',
                    address: '1234 Main St.',
                    address2: 'Suite 100',
                    city: 'Beverly Hills',
                    state: 'CA',
                    postal: '90210',
                    country: 'US',
                    phone: '555-555-5555'
                },
                orderInfo: {
                    shippingAmount: 0,
                    totalAmount: 12.00
                }
            };

            return retVal;
        }

        // From: https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
        function uuidv4() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    }
})();
