﻿(function () {
    'use strict';

    angular
        .module('app.solupay')
        .component('solupayTransaction', {
            templateUrl: 'app/solupay/threeStep/transaction.component.html',
            bindings: {},
            controllerAs: 'vm',
            controller: ['datacontext', 'threeStepService', controller]
        });
    
    function controller(datacontext, threeStepService) {
        var vm = this;
        vm.showOrderInfo = true;

        vm.$onInit = function() {
            vm.postData = threeStepService.getMockTransactionData();
        };

        vm.submit = function () {
            vm.submittingStep1 = true;

            return datacontext.postResource('SolupayTransaction/Step1', vm.postData).then(function (step1Result) {
                vm.submittingStep1 = false;
                vm.submittingStep2 = true;
                document.paymentForm.action = step1Result.formUrl;
                document.paymentForm.submit();
            });
        }
    }
}());
