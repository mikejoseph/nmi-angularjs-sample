﻿(function () {
    'use strict';

    angular
        .module('app.solupay')
        .component('threeStepResult', {
            templateUrl: 'app/solupay/threeStep/result.component.html',
            bindings: {
                // variable: '<'
            },
            controllerAs: 'vm',
            controller: ['$stateParams', 'datacontext', controller]
        });

    function controller($stateParams, datacontext) {
        var vm = this;
        
        vm.$onInit = function () {
            if (!$stateParams.tokenId) {
                vm.error = 'No token id was found.';
                return;
            }

            datacontext.getResource('SolupayTransaction/TransactionResultLog?tokenId=' + $stateParams.tokenId).then(function(data) {
                vm.resultLog = data;
            });
        };
    }
}());
