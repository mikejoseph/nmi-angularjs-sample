﻿(function () {
    'use strict';

    angular
        .module('app.solupay')
        .component('query', {
            templateUrl: 'app/solupay/query/query.component.html',
            bindings: {},
            controllerAs: 'vm',
            controller: ['$scope', 'datacontext', controller]
        });

    function controller($scope, datacontext) {
        var vm = this;

        $scope.$on('searchButtonClicked',
            function(event, data) {
                vm.searchVariables = data;
                refreshResults();
            });

        function refreshResults() {
            vm.error = null;
            vm.loading = true;
            
            datacontext.postResource('SolupayQuery/PerformSearch', vm.searchVariables).then(function (data) {
                vm.loading = false;
                vm.searchResults = data.transaction;
                vm.sourceUrl = data.sourceUrl;
            }, function (reason) {
                vm.loading = false;
                vm.error = reason;
            });
        }
    }
}());
