﻿(function () {
    'use strict';

    angular
        .module('app.solupay')
        .component('queryResults', {
            templateUrl: 'app/solupay/query/queryResults.component.html',
            bindings: {
                searchResults: '<',
                sourceUrl: '<'
            },
            controllerAs: 'vm',
            controller: ['$state', 'alertModal', controller]
        });

    function controller($state, alertModal) {
        var vm = this;
        vm.currentPage = 1;
        vm.totalPages = 0;
        vm.itemsPerPage = 3;
        vm.showReportRow = showReportRow;
        vm.onPageChanged = onPageChanged;
        vm.backToForm = backToForm;
        vm.showDetailsModal = showDetailsModal;

        vm.$onChanges = function () {
            if (!vm.searchResults) return;
            vm.currentPage = 1;
            vm.totalItems = vm.searchResults.length;
            vm.numPages = Math.floor(vm.searchResults.length / vm.itemsPerPage);
        };

        function showReportRow(index) {
            var startPosition = (vm.currentPage - 1) * vm.itemsPerPage;
            var endPosition = startPosition + vm.itemsPerPage - 1;
            return index >= startPosition && index <= endPosition;
        }

        function onPageChanged() {
            console.log('Page changed to ' + vm.currentPage);
        }

        function backToForm() {
            $state.reload();
        }

        function showDetailsModal(result) {
            var html = '<pre>' + JSON.stringify(result, null, 4) + '</pre>';
            alertModal(html, 'Showing Transaction Details');
        }
    }
}());
