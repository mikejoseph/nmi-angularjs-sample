﻿(function () {
    'use strict';

    angular
        .module('app.solupay')
        .component('queryForm', {
            templateUrl: 'app/solupay/query/queryForm.component.html',
            bindings: {},
            controllerAs: 'vm',
            controller: ['$scope', 'datacontext', controller]
        });

    function controller($scope, datacontext) {
        var vm = this;
        vm.searchVariables = {};

        vm.$onInit = function () {
            datacontext.getResource("SolupayQuery/QueryOptions").then(function (data) {
                vm.searchOptions = data;
                const date = new Date();
                vm.searchVariables.startDate = new Date(date.getFullYear(), date.getMonth(), 1);
                vm.searchVariables.endDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            });
        };

        vm.performSearch = function () {
            $scope.$emit('searchButtonClicked', vm.searchVariables);
        };
    }
}());
