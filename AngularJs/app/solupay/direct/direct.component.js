﻿(function () {
    'use strict';

    angular
        .module('app.solupay')
        .component('direct', {
            templateUrl: 'app/solupay/direct/direct.component.html',
            bindings: {},
            controllerAs: 'vm',
            controller: ['datacontext', controller]
        });

    function controller(datacontext) {
        var vm = this;
        vm.outputType = 'url';

        vm.$onInit = function () {
            vm.postData = {
                amount: 12,
                customerVaultId: '12345',
                transactionType: 'sale'
            };

            vm.deleteVault = {
                customerVaultId: '12345'
            };
        };

        vm.submit = function () {
            vm.transactionResult = null;
            vm.submitting = true;

            var resource = 'SolupayDirect/RunTransaction';
            
            datacontext.postResource(resource, vm.postData).then(function (data) {
                vm.transactionResult = data;
                vm.submitting = false;
            }, function(reason) {
                console.log(reason);
                vm.submitting = false;
            });
        }

        vm.removeVault = function() {
            vm.removingVault = true;
            var resource = 'SolupayDirect/RemoveCustomerVault';

            datacontext.postResource(resource, vm.deleteVault).then(function(data) {
                    vm.removeResult = data;
                    vm.removingVault = false;
                },
                function(reason) {
                    console.log(reason);
                    vm.removingVault = false;
                });
        };
    }
}());
