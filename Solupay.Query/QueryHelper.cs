﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Solupay.Query.Models;
using Solupay.Query.Models.Response;

namespace Solupay.Query
{
    public static class QueryHelper
    {

        public static NmResponse PerformSearch(SearchVariables searchVariables)
        {
            var url = GetSearchUrl(searchVariables);
            return PerformSearch(url);
        }

        public static NmResponse PerformSearch(string url)
        {
            var ser = new System.Xml.Serialization.XmlSerializer(typeof(NmResponse));
            var client = new System.Net.WebClient();
            // client.Headers.Add("Accept-Language: fi"); // header which determines the return language if google supports it
            var data = Encoding.Default.GetString(client.DownloadData(url));
            var stream = new System.IO.MemoryStream(Encoding.UTF8.GetBytes(data));
            var response = (NmResponse)ser.Deserialize(stream);
            return response;
        }

        #region GetSearchVariableOptions

        public static dynamic GetSearchVariableOptions()
        {
            return new
            {
                Conditions = new List<string>
                {
                    "pending",
                    "pendingsettlement",
                    "in_progress",
                    "abandoned",
                    "failed",
                    "canceled",
                    "complete",
                    "unknown"
                },
                TransactionType = new List<string>
                {
                    "cc",
                    "ck"
                },
                ActionType = new List<string>
                {
                    "sale",
                    "refund",
                    "credit",
                    "auth",
                    "capture",
                    "void",
                    "return"
                },
                Source = new List<string>
                {
                    "api",
                    "batch_upload",
                    "mobile",
                    "quickclick",
                    "quickbooks",
                    "recurring",
                    "swipe",
                    "virtual_terminal",
                    "internal"
                },
                ReportType = new List<string>
                {
                    "customer_vault",
                    "gateway_processors"
                }
            };
        }

        #endregion

        #region GetSearchUrl

        public static string GetSearchUrl(SearchVariables searchVariables)
        {
            var sb = new StringBuilder();
            sb.Append(ConfigurationManager.AppSettings["SolupayQueryApiUrl"]);
            sb.Append($"?username={ConfigurationManager.AppSettings["SolupayDirectApiUsername"]}&password={ConfigurationManager.AppSettings["SolupayDirectApiPassword"]}");
            AppendStringListParameter(sb, "condition", searchVariables.Condition);
            AppendStringParameter(sb, "transaction_type", searchVariables.TransactionType);
            AppendStringListParameter(sb, "action_type", searchVariables.ActionType);
            AppendStringListParameter(sb, "source", searchVariables.Source);
            AppendStringParameter(sb, "transaction_id", searchVariables.TransactionId);
            AppendStringParameter(sb, "transaction_type", searchVariables.TransactionType);
            AppendStringParameter(sb, "partial_payment_id", searchVariables.PartialPaymentId);
            AppendStringParameter(sb, "order_id", searchVariables.OrderId);
            AppendStringParameter(sb, "last_name", searchVariables.LastName);
            AppendStringParameter(sb, "email", searchVariables.Email);
            AppendStringParameter(sb, "cc_number", searchVariables.CcNumber);
            AppendDateParameter(sb, "start_date", searchVariables.StartDate);
            AppendDateParameter(sb, "end_date", searchVariables.EndDate);
            AppendStringListParameter(sb, "report_type", searchVariables.ReportType);
            AppendStringParameter(sb, "mobile_device_license", searchVariables.MobileDeviceLicense);
            AppendStringParameter(sb, "mobile_device_nickname", searchVariables.MobileDeviceNickname);
            AppendStringParameter(sb, "customer_vault_id", searchVariables.CustomerVaultId);
            return sb.ToString();
        }

        private static void AppendStringListParameter(StringBuilder sb, string keyName, IList<string> stringList)
        {
            if (stringList == null || stringList.Count == 0) return;
            sb.Append($"&{keyName}=");

            for (var i = 0; i < stringList.Count; i++)
            {
                sb.Append(Uri.EscapeDataString(stringList[i]));
                if (i + 1 != stringList.Count) sb.Append(",");
            }
        }

        private static void AppendStringParameter(StringBuilder sb, string keyName, string value)
        {
            if (string.IsNullOrEmpty(value)) return;
            sb.Append($"&{keyName}={Uri.EscapeDataString(value)}");
        }

        private static void AppendDateParameter(StringBuilder sb, string keyName, DateTime? value)
        {
            if (!value.HasValue) return;
            var d = value.Value;
            var dateFormatted =
                $"{d.Year}{ConvertTo2Digits(d.Month)}{ConvertTo2Digits(d.Day)}" +
                $"{ConvertTo2Digits(d.Hour)}{ConvertTo2Digits(d.Minute)}{ConvertTo2Digits(d.Second)}";
            sb.Append($"&{keyName}={dateFormatted}");
        }

        private static string ConvertTo2Digits(int i)
        {
            return i.ToString().Length == 1 ? $"0{i}" : i.ToString();
        }

        #endregion

    }
}
