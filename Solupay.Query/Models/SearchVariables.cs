﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace Solupay.Query.Models
{
    /// <summary>
    /// The documentation for these search variables came from:
    /// https://secure.networkmerchants.com/gw/merchants/resources/integration/integration_portal.php#query_variables
    /// </summary>
    public class SearchVariables
    {
        /// <summary>
        /// Merchant username.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Merchant password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// A combination of values listed below can be passed and should be separated by commas. For example, to retrieve all transactions pending settlement or complete, the following could be used:
        /// 
        /// Example: condition=pendingsettlement,complete
        /// pending: 'Auth Only' transactions that are awaiting capture.
        /// pendingsettlement: This transaction is awaiting settlement.
        /// in_progress: This Three-Step Redirect API transaction has not yet been completed. The transaction condition will change to 'abandoned' if 24 hours pass with no further action.
        /// abandoned: This Three-Step Redirect API transaction has not been completed, and has timed out.
        /// failed: This transaction has failed.
        /// canceled: This transaction has been voided.
        /// complete: This transaction has settled.
        /// unknown: An unknown error was encountered while processing this transaction.
        /// </summary>
        public IList<string> Condition { get; set; }

        /// <summary>
        /// Retrieves only transactions with the specified transaction type. Use one of the following to specify payment type:
        /// cc: A credit card transaction.
        /// ck: A check transaction.
        /// </summary>
        public string TransactionType { get; set; }

        /// <summary>
        /// action_type
        /// 
        /// Retrieves only transactions with the specified action types. A combination of the values can be used and should be separated by commas. For example, to retrieve all transactions with credit or refund actions, use the following:
        /// 
        /// Example: action_type=refund,credit
        /// sale: Sale transactions.
        /// refund: Refund transactions.
        /// credit: Credit transactions.
        /// auth: 'Auth Only' transactions.
        /// capture: Captured transactions.
        /// void: Voided transactions.
        /// return: Electronic Check (ACH) transactions that have returned before (return) or after settlement (late_return).
        /// </summary>
        public IList<string> ActionType { get; set; }

        /// <summary>
        /// source 	Retrieves only transactions with a particular 'transaction source'. A combination of the values can be used and should be separated by commas. For example, to retrieve all transactions with api or recurring actions, use the following:
        /// 
        /// Example: source=api,recurring
        /// api: API transactions.
        /// batch_upload: Batch Upload transactions.
        /// mobile: Mobile (iProcess) transactions.
        /// quickclick: QuickClick transactions.
        /// quickbooks: QuickBooks SyncPay transactions.
        /// recurring: Recurring transactions when using Recurring module.
        /// swipe: SwIPe Software transactions.
        /// virtual_terminal: Virtual Terminal transactions.
        /// internal: Internal transactions. Typically indicates settlement
        /// </summary>
        public IList<string> Source { get; set; }

        /// <summary>
        /// transaction_id 	Specify a transaction ID or a comma separated list of transaction IDs to retrieve information on. Alternatively, provide a Subscription ID to retrieve processed (approved and declined) transactions associated with it.
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// partial_payment_id 	Retrieves only transactions with the specified partial payment ID.
        /// </summary>
        public string PartialPaymentId { get; set; }

        /// <summary>
        /// order_id 	Retrieves only transactions with the specified Order ID.
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// last_name 	Retrieves only transactions with the specified last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// email 	Retrieves only transactions with the specified billing email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// cc_number 	Retrieves only transactions with the specified credit card number. You can use either the full number or the last 4 digits of the credit card number.
        /// </summary>
        public string CcNumber { get; set; }

        ///// TODO: Make this more dynamic to allow for the 1-20...
        ///// <summary>
        ///// merchant_defined_field_# 	Retrieves only transactions with the specified merchant defined field value.
        ///// Replace the '#' with a field number (1-20) (Example: merchant_defined_field_12=value)
        ///// </summary>
        //public string MerchantDefinedField1 { get; set; }

        /// <summary>
        /// start_date 	Only transactions that have been modified on or after this date will be retrieved. Note that any actions performed on a transaction will cause the modified date to be updated.
        /// Format: YYYYMMDDhhmmss
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// end_date 	Only transactions that have been modified on or before this date will be retrieved. Note that any actions performed on a transaction will cause the modified date to be updated.
        /// Format: YYYYMMDDhhmmss
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// report_type 	Allows customer vault information or a html receipt to be returned. If you would like to query the Customer Vault to view what customer information is stored in the Customer Vault, you must set the customer_vault variable.
        /// If you omit the customer_vault_id, the system will return all customers that are stored in the vault. If you include a customer_vault_id, it will return the customer record associated with that ID.
        /// Example: report_type=customer_vault&customer_vault_id=123456789
        /// receipt: Will return an html receipt for a specified transaction id.
        /// customer_vault: Set the Query API to return Customer Vault data.
        /// gateway_processors: Will return Processor details a user has permissions for. Specify a "user" by querying with that username/password.
        /// </summary>
        public IList<string> ReportType { get; set; }

        /// <summary>
        /// mobile_device_license 	Retrieves only transactions processed using the specified mobile device.
        /// The device IDs for this parameter are available in the License Manager.
        /// Use 'any_mobile' to retrieve all mobile transactions.
        /// A combination of the values can be used and should be separated by commas.
        /// Can not be used with 'mobile_device_nickname'.
        /// 
        /// Example 1: mobile_device_license=D91AC56A-4242-3131-2323-2AE4AA6DB6EB
        /// Example 2: mobile_device_license=any_mobile
        /// </summary>
        public string MobileDeviceLicense { get; set; }

        /// <summary>
        /// mobile_device_nickname 	Retrieves only transactions processed using mobile devices with the specified nickname.
        /// The nicknames for this parameter are available in the License Manager.
        /// Can not be used with 'mobile_device_license'.
        /// 
        /// Example (URL encoded): mobile_device_nickname=Jim's%20iPhone
        /// </summary>
        public string MobileDeviceNickname { get; set; }

        /// <summary>
        /// customer_vault_id 	Set a specific Customer Vault record. Should only be used when report_type=customer_vault.
        /// </summary>
        public string CustomerVaultId { get; set; }

        public SearchVariables()
        {
            Condition = new List<string>();
            ActionType = new List<string>();
            Source = new List<string>();
            ReportType = new List<string>();
        }
    }
}
