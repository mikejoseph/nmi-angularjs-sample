using System.Xml.Serialization;

namespace Solupay.Query.Models.Response
{
    [XmlRoot(ElementName = "action")]
    public class Action
    {
        [XmlElement(ElementName = "amount")]
        public string Amount { get; set; }

        [XmlElement(ElementName = "action_type")]
        public string ActionType { get; set; }

        [XmlElement(ElementName = "date")]
        public string Date { get; set; }

        [XmlElement(ElementName = "success")]
        public string Success { get; set; }

        [XmlElement(ElementName = "ip_address")]
        public string IpAddress { get; set; }

        [XmlElement(ElementName = "source")]
        public string Source { get; set; }

        [XmlElement(ElementName = "username")]
        public string Username { get; set; }

        [XmlElement(ElementName = "response_text")]
        public string ResponseText { get; set; }

        [XmlElement(ElementName = "batch_id")]
        public string BatchId { get; set; }

        [XmlElement(ElementName = "processor_batch_id")]
        public string ProcessorBatchId { get; set; }

        [XmlElement(ElementName = "response_code")]
        public string ResponseCode { get; set; }

        [XmlElement(ElementName = "processor_response_text")]
        public string ProcessorResponseText { get; set; }

        [XmlElement(ElementName = "processor_response_code")]
        public string ProcessorResponseCode { get; set; }

        [XmlElement(ElementName = "requested_amount")]
        public string RequestedAmount { get; set; }

        [XmlElement(ElementName = "device_license_number")]
        public string DeviceLicenseNumber { get; set; }

        [XmlElement(ElementName = "device_nickname")]
        public string DeviceNickname { get; set; }
    }
}