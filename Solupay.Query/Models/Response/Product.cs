using System.Xml.Serialization;

namespace Solupay.Query.Models.Response
{
    [XmlRoot(ElementName = "product")]
    public class Product
    {
        [XmlElement(ElementName = "sku")]
        public string Sku { get; set; }

        [XmlElement(ElementName = "quantity")]
        public string Quantity { get; set; }

        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

        [XmlElement(ElementName = "amount")]
        public string Amount { get; set; }
    }
}