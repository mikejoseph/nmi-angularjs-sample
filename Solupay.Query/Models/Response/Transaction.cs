using System.Collections.Generic;
using System.Xml.Serialization;

namespace Solupay.Query.Models.Response
{
    [XmlRoot(ElementName = "transaction")]
    public class Transaction
    {
        [XmlElement(ElementName = "transaction_id")]
        public string TransactionId { get; set; }

        [XmlElement(ElementName = "partial_payment_id")]
        public string PartialPaymentId { get; set; }

        [XmlElement(ElementName = "partial_payment_balance")]
        public string PartialPaymentBalance { get; set; }

        [XmlElement(ElementName = "platform_id")]
        public string PlatformId { get; set; }

        [XmlElement(ElementName = "transaction_type")]
        public string TransactionType { get; set; }

        [XmlElement(ElementName = "condition")]
        public string Condition { get; set; }

        [XmlElement(ElementName = "order_id")]
        public string OrderId { get; set; }

        [XmlElement(ElementName = "authorization_code")]
        public string AuthorizationCode { get; set; }

        [XmlElement(ElementName = "ponumber")]
        public string Ponumber { get; set; }

        [XmlElement(ElementName = "order_description")]
        public string OrderDescription { get; set; }

        [XmlElement(ElementName = "first_name")]
        public string FirstName { get; set; }

        [XmlElement(ElementName = "last_name")]
        public string LastName { get; set; }

        [XmlElement(ElementName = "address_1")]
        public string Address1 { get; set; }

        [XmlElement(ElementName = "address_2")]
        public string Address2 { get; set; }

        [XmlElement(ElementName = "company")]
        public string Company { get; set; }

        [XmlElement(ElementName = "city")]
        public string City { get; set; }

        [XmlElement(ElementName = "state")]
        public string State { get; set; }

        [XmlElement(ElementName = "postal_code")]
        public string PostalCode { get; set; }

        [XmlElement(ElementName = "country")]
        public string Country { get; set; }

        [XmlElement(ElementName = "email")]
        public string Email { get; set; }

        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }

        [XmlElement(ElementName = "fax")]
        public string Fax { get; set; }

        [XmlElement(ElementName = "cell_phone")]
        public string CellPhone { get; set; }

        [XmlElement(ElementName = "customertaxid")]
        public string Customertaxid { get; set; }

        [XmlElement(ElementName = "customerid")]
        public string Customerid { get; set; }

        [XmlElement(ElementName = "website")]
        public string Website { get; set; }

        [XmlElement(ElementName = "shipping_first_name")]
        public string ShippingFirstName { get; set; }

        [XmlElement(ElementName = "shipping_last_name")]
        public string ShippingLastName { get; set; }

        [XmlElement(ElementName = "shipping_address_1")]
        public string ShippingAddress1 { get; set; }

        [XmlElement(ElementName = "shipping_address_2")]
        public string ShippingAddress2 { get; set; }

        [XmlElement(ElementName = "shipping_company")]
        public string ShippingCompany { get; set; }

        [XmlElement(ElementName = "shipping_city")]
        public string ShippingCity { get; set; }

        [XmlElement(ElementName = "shipping_state")]
        public string ShippingState { get; set; }

        [XmlElement(ElementName = "shipping_postal_code")]
        public string ShippingPostalCode { get; set; }

        [XmlElement(ElementName = "shipping_country")]
        public string ShippingCountry { get; set; }

        [XmlElement(ElementName = "shipping_email")]
        public string ShippingEmail { get; set; }

        [XmlElement(ElementName = "shipping_carrier")]
        public string ShippingCarrier { get; set; }

        [XmlElement(ElementName = "tracking_number")]
        public string TrackingNumber { get; set; }

        [XmlElement(ElementName = "shipping_date")]
        public string ShippingDate { get; set; }

        [XmlElement(ElementName = "shipping")]
        public string Shipping { get; set; }

        [XmlElement(ElementName = "shipping_phone")]
        public string ShippingPhone { get; set; }

        [XmlElement(ElementName = "cc_number")]
        public string CcNumber { get; set; }

        [XmlElement(ElementName = "cc_hash")]
        public string CcHash { get; set; }

        [XmlElement(ElementName = "cc_exp")]
        public string CcExp { get; set; }

        [XmlElement(ElementName = "cavv")]
        public string Cavv { get; set; }

        [XmlElement(ElementName = "cavv_result")]
        public string CavvResult { get; set; }

        [XmlElement(ElementName = "xid")]
        public string Xid { get; set; }

        [XmlElement(ElementName = "avs_response")]
        public string AvsResponse { get; set; }

        [XmlElement(ElementName = "csc_response")]
        public string CscResponse { get; set; }

        [XmlElement(ElementName = "cardholder_auth")]
        public string CardholderAuth { get; set; }

        [XmlElement(ElementName = "cc_start_date")]
        public string CcStartDate { get; set; }

        [XmlElement(ElementName = "cc_issue_number")]
        public string CcIssueNumber { get; set; }

        [XmlElement(ElementName = "check_account")]
        public string CheckAccount { get; set; }

        [XmlElement(ElementName = "check_hash")]
        public string CheckHash { get; set; }

        [XmlElement(ElementName = "check_aba")]
        public string CheckAba { get; set; }

        [XmlElement(ElementName = "check_name")]
        public string CheckName { get; set; }

        [XmlElement(ElementName = "account_holder_type")]
        public string AccountHolderType { get; set; }

        [XmlElement(ElementName = "account_type")]
        public string AccountType { get; set; }

        [XmlElement(ElementName = "sec_code")]
        public string SecCode { get; set; }

        [XmlElement(ElementName = "drivers_license_number")]
        public string DriversLicenseNumber { get; set; }

        [XmlElement(ElementName = "drivers_license_state")]
        public string DriversLicenseState { get; set; }

        [XmlElement(ElementName = "drivers_license_dob")]
        public string DriversLicenseDob { get; set; }

        [XmlElement(ElementName = "social_security_number")]
        public string SocialSecurityNumber { get; set; }

        [XmlElement(ElementName = "processor_id")]
        public string ProcessorId { get; set; }

        [XmlElement(ElementName = "tax")]
        public string Tax { get; set; }

        [XmlElement(ElementName = "currency")]
        public string Currency { get; set; }

        [XmlElement(ElementName = "surcharge")]
        public string Surcharge { get; set; }

        [XmlElement(ElementName = "tip")]
        public string Tip { get; set; }

        [XmlElement(ElementName = "card_balance")]
        public string CardBalance { get; set; }

        [XmlElement(ElementName = "card_available_balance")]
        public string CardAvailableBalance { get; set; }

        [XmlElement(ElementName = "entry_mode")]
        public string EntryMode { get; set; }

        [XmlElement(ElementName = "cc_bin")]
        public string CcBin { get; set; }

        [XmlElement(ElementName = "product")]
        public Product Product { get; set; }

        [XmlElement(ElementName = "action")]
        public List<Action> Action { get; set; }
    }
}