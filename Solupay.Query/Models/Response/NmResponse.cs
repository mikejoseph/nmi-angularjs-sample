using System.Collections.Generic;
using System.Xml.Serialization;

namespace Solupay.Query.Models.Response
{
    [XmlRoot(ElementName = "nm_response")]
    public class NmResponse
    {
        [XmlElement(ElementName = "transaction")]
        public List<Transaction> Transaction { get; set; }

        public string SourceUrl { get; set; }
    }
}