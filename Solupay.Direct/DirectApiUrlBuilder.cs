﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Solupay.Direct.Models;

namespace Solupay.Direct
{
    public class DirectApiUrlBuilder
    {
        public string BuildUrl(TransactionInputModel model)
        {
            var sb = new StringBuilder();
            sb.Append($"{ConfigurationManager.AppSettings["SolupayDirectApiUrl"]}?");
            sb.Append(BuildPostString(model));
            return sb.ToString();
        }

        public string BuildPostString(TransactionInputModel model)
        {
            var sb = new StringBuilder();
            sb.Append($"username={Uri.EscapeDataString(ConfigurationManager.AppSettings["SolupayDirectApiUsername"])}");
            sb.Append($"&password={Uri.EscapeDataString(ConfigurationManager.AppSettings["SolupayDirectApiPassword"])}");
            sb.Append($"&type={Uri.EscapeDataString(model.TransactionType)}");
            sb.Append($"&amount={Uri.EscapeDataString(model.Amount.ToString("N").Replace(",", ""))}");
            sb.Append($"&customer_vault_id={Uri.EscapeDataString(model.CustomerVaultId)}");
            sb.Append($"&orderid={model.OrderId}");
            if (model.MerchantDefinedFields == null) return sb.ToString();

            var i = 1;
            foreach (var field in model.MerchantDefinedFields)
            {
                sb.Append($"&merchant_defined_field_{i}={field}");
                i++;
            }

            return sb.ToString();
        }

        public string BuildPostString(RemoveCustomerVaultModel model)
        {
            var sb = new StringBuilder();
            sb.Append($"customer_vault=delete_customer&username={Uri.EscapeDataString(ConfigurationManager.AppSettings["SolupayDirectApiUsername"])}");
            sb.Append($"&password={Uri.EscapeDataString(ConfigurationManager.AppSettings["SolupayDirectApiPassword"])}");
            sb.Append($"&customer_vault_id={Uri.EscapeDataString(model.CustomerVaultId)}");
            return sb.ToString();
        }

        public string GetResultString(TransactionInputModel model)
        {
            var url = ConfigurationManager.AppSettings["SolupayDirectApiUrl"];
            var postData = BuildPostString(model);
            var resultString = PostRequest(url, postData);
            return resultString;
        }

        public string GetResultString(RemoveCustomerVaultModel model)
        {
            var url = ConfigurationManager.AppSettings["SolupayDirectApiUrl"];
            var postData = BuildPostString(model);
            var resultString = PostRequest(url, postData);
            return resultString;
        }

        private string PostRequest(string url, string postData)
        {
            var objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Method = "POST";
            objRequest.ContentLength = postData.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            StreamWriter myWriter = null;

            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(postData);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                myWriter?.Close();
            }

            string result;
            var objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (var sr = new StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
                sr.Close();
            }

            return result;
        }
    }
}
