﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solupay.Direct.Models
{
    public class TransactionInputModel
    {
        // Maps to type
        public string TransactionType { get; set; }

        // Maps to customer_vault_id
        public string CustomerVaultId { get; set; }

        // Maps to amount
        public decimal Amount { get; set; }

        public IList<string> MerchantDefinedFields { get; set; }

        // Maps to orderid
        public string OrderId { get; set; }
    }
}
