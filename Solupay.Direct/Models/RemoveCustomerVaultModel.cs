namespace Solupay.Direct.Models
{
    public class RemoveCustomerVaultModel
    {
        // Maps to customer_vault
        public string CustomerVault { get; set; }

        // Maps to customer_vault_id
        public string CustomerVaultId { get; set; }

        public RemoveCustomerVaultModel()
        {
            CustomerVault = "delete_customer";
        }
    }
}