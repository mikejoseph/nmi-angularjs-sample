﻿using System;

namespace WebApi.Models
{
    public class TransactionResultLogDescription
    {
        public string TokenId { get; set; }

        public DateTime DateAdded { get; set; }

        public long Bytes { get; set; }
    }
}