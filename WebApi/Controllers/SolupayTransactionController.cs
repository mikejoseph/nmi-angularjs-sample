﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Solupay.Common;
using Solupay.ThreeStep;
using Solupay.ThreeStep.Models;
using WebApi.Models;
using Formatting = Newtonsoft.Json.Formatting;

namespace WebApi.Controllers
{
    [RoutePrefix("api/SolupayTransaction")]
    public class SolupayTransactionController : ApiController
    {
        
        [HttpPost]
        [Route("Step1")]
        [ResponseType(typeof(Step1OutputModel))]
        public IHttpActionResult Post([FromBody]Step1Model model)
        {
            model.Step2RedirectUrl = ConfigurationManager.AppSettings["SolupayStep2RedirectUrl"];
            var requestBuilder = new SolupayXmlRequestBuilder(ConfigurationManager.AppSettings["SolupayApiKey"]);
            var xmlRequest = requestBuilder.GetTransactionStep1Request(model);
            LogHelper.LogXml("Step1Request", xmlRequest);

            var responseFromServer = SolupayRequestHelper.SendXmlRequest(xmlRequest);
            LogHelper.LogXml("Step1Response", responseFromServer);

            var responseReader = XmlReader.Create(new StringReader(responseFromServer));
            var serializer = new XmlSerializer(typeof(Step1OutputModel));
            var result = (Step1OutputModel)serializer.Deserialize(responseReader);
            return Ok(result);
        }

        [HttpGet]
        [Route("TransactionResult")]
        public IHttpActionResult TransactionResult(bool showPath = false)
        {
            var request = HttpContext.Current.Request;
            if (request["token-id"] == null) return BadRequest("Expected a token-id parameter.");

            var requestBuilder = new SolupayXmlRequestBuilder(ConfigurationManager.AppSettings["SolupayApiKey"]);
            var xmlRequest = requestBuilder.GetCompleteTransactionRequest(request["token-id"]);
            LogHelper.LogXml("Step3Request", xmlRequest);

            var responseFromServer = SolupayRequestHelper.SendXmlRequest(xmlRequest);
            LogHelper.LogXml("Step3Response", responseFromServer);
            var responseReader = XmlReader.Create(new StringReader(responseFromServer));
            var serializer = new XmlSerializer(typeof(Step3OutputModel));
            var result = (Step3OutputModel)serializer.Deserialize(responseReader);

            // Write the output results to a log file - You would probably want to log this to a local database too.
            var logFolder = ConfigurationManager.AppSettings["SolupayTransactionLogFolder"];
            if (!Directory.Exists(logFolder)) Directory.CreateDirectory(logFolder);
            var savePath = $"{logFolder}{request["token-id"]}.json";
            File.WriteAllText(savePath, JsonConvert.SerializeObject(result, Formatting.Indented));

            // If the CustomerVaultId is specified, write that info to the database:
            if (!string.IsNullOrEmpty(result.CustomerVaultId) && result.Result == 1)
            {
                // TODO: Insert database code here.
            }

            // Redirect to the client app w/ the token id so that the client app can get the log data:
            var url = $"{ConfigurationManager.AppSettings["TransactionResultClientUrl"]}{request["token-id"]}";

            if (showPath) return Ok(url);
            return Redirect(url);
        }
        
        [ResponseType(typeof(Step3OutputModel))]
        public IHttpActionResult Get(string tokenId)
        {
            var filePath = $"{ConfigurationManager.AppSettings["SolupayTransactionLogFolder"]}{tokenId}.json";
            if (!File.Exists(filePath)) return NotFound();
            return Ok(JsonConvert.DeserializeObject<Step3OutputModel>(File.ReadAllText(filePath)));
        }

        #region TransactionLog Endpoints

        [HttpGet]
        [Route("TransactionLog")]
        [ResponseType(typeof(List<TransactionResultLogDescription>))]
        public IHttpActionResult GetTransactionResultLogs()
        {
            var directory = ConfigurationManager.AppSettings["SolupayTransactionLogFolder"];
            var retVal = new List<TransactionResultLogDescription>();
            if (!Directory.Exists(directory)) return Ok(retVal);

            foreach (var file in new DirectoryInfo(directory).GetFiles("*.json"))
            {
                retVal.Add(new TransactionResultLogDescription
                {
                    TokenId = file.Name.Replace(".json", ""),
                    Bytes = file.Length,
                    DateAdded = file.CreationTime
                });
            }

            return Ok(retVal.OrderByDescending(x => x.DateAdded).ToList());
        }

        [HttpGet]
        [Route("TransactionLog")]
        [ResponseType(typeof(List<TransactionResultLogDescription>))]
        public IHttpActionResult GetTransactionResultLog(string tokenId)
        {
            var logFolder = ConfigurationManager.AppSettings["SolupayTransactionLogFolder"];
            var logPath = $"{logFolder}{tokenId}.json";
            if (!File.Exists(logPath)) return BadRequest($"The file {logPath} was not found.");
            var log = JsonConvert.DeserializeObject<Step3OutputModel>(File.ReadAllText(logPath));
            return Ok(log);
        }

        [HttpDelete]
        [Route("TransactionLog")]
        public IHttpActionResult DeleteTransactionResultLogs()
        {
            var directory = ConfigurationManager.AppSettings["SolupayTransactionLogFolder"];
            foreach (var fileInfo in new DirectoryInfo(directory).GetFiles("*.json"))
            {
                File.Delete(fileInfo.FullName);
            }

            return Ok();
        }

        #endregion
    }
}
