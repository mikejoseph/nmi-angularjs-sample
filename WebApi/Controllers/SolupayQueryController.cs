﻿using System.Web.Http;
using System.Web.Http.Description;
using Solupay.Query;
using Solupay.Query.Models;
using Solupay.Query.Models.Response;

namespace WebApi.Controllers
{
    [RoutePrefix("api/SolupayQuery")]
    public class SolupayQueryController : ApiController
    {
        /// <summary>
        /// Returns the URL that the POST endpoint uses to perform its search.
        /// </summary>
        /// <param name="searchVariables"></param>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [HttpGet]
        [Route("SearchUrl")]
        public IHttpActionResult Get([FromBody] SearchVariables searchVariables)
        {
            return Ok(QueryHelper.GetSearchUrl(searchVariables));
        }

        /// <summary>
        /// Performs a search using the Solupay Query API.
        /// </summary>
        /// <param name="searchVariables"></param>
        /// <returns></returns>
        [ResponseType(typeof(NmResponse))]
        [HttpPost]
        [Route("PerformSearch")]
        public IHttpActionResult Post([FromBody] SearchVariables searchVariables)
        {
            var url = QueryHelper.GetSearchUrl(searchVariables);
            var results = QueryHelper.PerformSearch(url);
            results.SourceUrl = url;
            return Ok(results);
        }

        /// <summary>
        /// This endpoint returns a list of search options for use with
        /// the Solupay Query API.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("QueryOptions")]
        public IHttpActionResult QueryOptions()
        {
            return Ok(QueryHelper.GetSearchVariableOptions());
        }
    }
}
