﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using Solupay.Common;
using Solupay.Direct;
using Solupay.Direct.Models;

namespace WebApi.Controllers
{
    [RoutePrefix("api/SolupayDirect")]
    public class SolupayDirectController : ApiController
    {
        [HttpPost]
        [Route("RunTransaction")]
        [ResponseType(typeof(object))]
        public IHttpActionResult RunTransaction([FromBody]TransactionInputModel model)
        {
            var directApiHelper = new DirectApiUrlBuilder();
            var url = directApiHelper.BuildUrl(model);
            LogHelper.LogText("DirectTransactionRequest", url);

            var resultString = directApiHelper.GetResultString(model);
            LogHelper.LogText("DirectTransactionResponse", url);

            var returnObject = ResultStringToJObject(resultString);
            return Ok(returnObject);
        }

        [HttpPost]
        [Route("RemoveCustomerVault")]
        [ResponseType(typeof(object))]
        public IHttpActionResult RemoveCustomerVault([FromBody]RemoveCustomerVaultModel model)
        {
            var directApiHelper = new DirectApiUrlBuilder();
            var resultString = directApiHelper.GetResultString(model);
            var returnObject = ResultStringToJObject(resultString);
            return Ok(returnObject);
        }

        private static JObject ResultStringToJObject(string resultString)
        {
            var items = resultString.Split('&');
            var returnObject = new JObject();

            foreach (var item in items)
            {
                var a = item.Split('=');
                var property = new JProperty(a[0], Uri.UnescapeDataString(a[1]));
                returnObject.Add(property);
            }
            return returnObject;
        }
    }
}
