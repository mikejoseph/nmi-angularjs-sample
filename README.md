# NMI Gateway POC

>* This project is maintained by Mike Joseph [@mikejoseph23](//twitter.com/mikejoseph23)*

This application is an example of a modern implementation of the NMI Payment Gateway using their 3-Step Redirect, Direct and Query APIs.
Further documentation on the API can be found here: https://www.nmi.com/


## Running the application

This application should be fairly easy to get up and running. 

1. Using Visual Studio Community edition (version 2012 or later), lad the Solupay-PoC.sln file.

2. Once the project loads for the first time, you will need to Build the Solution. Use the Ctrl-Shift-B command to do this. It should take about 30 or so seconds to complete on a semi-decent Internet connection.

3. Start the application - Since the application consists of two separate IIS Express websites, you will need to run both applications

    - Right-click on the WebApi project in the Solution Explorer and click "View" --> "View in Browser". This will launch the API website in IIS without debugging enabled.
    - Right-click on the AngularJs project in the Solution Explorer and click "View in Browser" to launch the client-side application.

That should be it!


## Overview

This Visual Studio solution is broken into two main parts.

1. An AngularJs frontend application

2. An ASP.NET WebApi2 backend application


### Angular JS Application

First, I started off by installing the hot-towel project by using the instructions on this page:
https://github.com/johnpapa/generator-hottowel

Then, I stripped away all of the node js stuff that was included in that and placed it in the AngularJs Website in the Visual Studio Solution.

All of the Solupay related AngularJs components are contained within the app/solupay folder of the AngularJs Website.

* NOTE: The example client application uses local storage to maintain session data and will not work in your browser's incognito or private mode.


### WebApi Application

This is a standard ASP.NET WebApi2 template. The API has been configured to allow for CORS requests. All of the Solupay related code in this project
is contained in the Controllers folder.


### Solupay.ThreeStep C# Library

This is an ASP.NET class library that contains all of the business logic utilized by the WebApi Application.




## License

MIT