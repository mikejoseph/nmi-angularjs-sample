﻿using System;
using System.Configuration;
using System.IO;
using System.Xml;

namespace Solupay.Common
{
    public class LogHelper
    {
        public static void LogXml(string type, string xmlBody)
        {
            var saveFilePath = GetSaveFilePath(type);
            File.WriteAllText(saveFilePath, xmlBody);
        }

        public static void LogXml(string type, XmlDocument xml)
        {
            if (!bool.Parse(ConfigurationManager.AppSettings["SolupayEnableVerboseLogging"])) return;
            var saveFilePath = GetSaveFilePath(type);
            xml.Save(saveFilePath);
        }

        public static void LogText(string type, string textToLog)
        {
            var saveFilePath = GetSaveFilePath(type);
            File.WriteAllText(saveFilePath, textToLog);
        }

        private static string GetSaveFilePath(string type)
        {
            var d = DateTime.UtcNow;
            var baseLogFolder = ConfigurationManager.AppSettings["SolupayXmlLogFolder"];
            if (!Directory.Exists(baseLogFolder)) Directory.CreateDirectory(baseLogFolder);
            var saveFolder = $"{baseLogFolder}{d.Year}-{To2Digits(d.Month)}\\";
            if (!Directory.Exists(saveFolder)) Directory.CreateDirectory(saveFolder);
            var saveFileName = $"{GetTimeStamp(d)}_{type}.xml";
            var saveFilePath = $"{saveFolder}{saveFileName}";
            return saveFilePath;
        }

        private static string GetTimeStamp(DateTime d)
        {
            return
                $"{d.Year}-{To2Digits(d.Month)}-{To2Digits(d.Day)} {To2Digits(d.Hour)}.{To2Digits(d.Hour)}.{To2Digits(d.Minute)}.{To2Digits(d.Second)}.{d.Millisecond}";
        }

        private static string To2Digits(int i)
        {
            return i.ToString().Length == 1 ? $"0{i}" : i.ToString();
        }
    }
}
